<?php
    use TechnikTomCZ\StagWeb\ApiClient as ApiClient;

    $rootPath = $_SERVER['DOCUMENT_ROOT'];
    $currentPage = trim($_SERVER['REQUEST_URI'], '\//');

    $pages = [
        'home' => 'Domů',
        'subjects' => 'Přemdměty',
        'rooms' => 'Místnosti',
        'thesis' => 'Diplomové práce'
    ];

    if(strlen($currentPage) === 0 || !array_key_exists($currentPage, $pages))
        $currentPage = 'home';

    include_once "../vendor/autoload.php";
    include_once "../src/ValueFunctions.php";
    ApiClient::InitInstance();

    $requestParams = array_filter($_POST, fn($v) => strlen($v) > 0);
    $requestCalled = isset($_POST['rqs_btn']);
    $isSomeParamsSet = sizeof($requestParams) > 0 && $requestCalled;

    include_once "$rootPath/../views/$currentPage.php";