<?php
    namespace TechnikTomCZ\StagWeb;

    class Nav {
        private array $entries;
        private string $appName = "StagWebApi";

        public function __construct(array $entries) {
            $this->entries = $entries;
        }

        public function render($activePage): string
        {
            $nav = '<nav class="navbar navbar-expand-lg navbar-light bg-light">';
                $nav .= '<div class="container-fluid">';
                    $nav .= '<a class="navbar-brand" href="#">'.$this->appName.'</a>';
                    $nav .= '<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">';
                        $nav .= '<span class="navbar-toggler-icon"></span>';
                    $nav .= '</button>';
                    $nav .= '<div class="collapse navbar-collapse" id="navbarNav">';
                        $nav .= '<ul class="navbar-nav">';
                            foreach ($this->entries as $key => $name) {
                                $nav .= '<li class="nav-item">';
                                    if($activePage === $key)
                                        $nav .= '<a class="nav-link active" aria-current="page" href="/'.$key.'">'.$name.'</a>';
                                    else
                                        $nav .= '<a class="nav-link" aria-current="page" href="/'.$key.'">'.$name.'</a>';
                                $nav .= '</li>';
                            }
                        $nav .= '</ul>';
                    $nav .= '</div>';
                $nav .= '</div>';
            $nav .= '</nav>';

            return $nav;
        }
    }
