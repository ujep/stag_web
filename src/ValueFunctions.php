<?php
function isSelected(string $propName, string $value): string
{
    global $requestParams;

    if (isset($requestParams[$propName]) && $requestParams[$propName] == $value) {
        return 'selected';
    }

    return '';
}

function noneIsSelected(string $propName): string
{
    global $requestParams;

    if (!isset($requestParams[$propName])) {
        return 'selected';
    }

    return '';
}

function enterValueIfExists($propertyName): string {
    global $requestParams;

    if(isset($requestParams[$propertyName])) {
        return 'value="'.$requestParams[$propertyName].'"';
    }

    return '';
}