<?php

namespace TechnikTomCZ\StagWeb;
class Calendar
{
    private const baseYear = 2020;

    public static function GetLastYears(int $count): array
    {
        if ($count <= 0)
            return [];

        $years = [self::baseYear];

        for ($i = 1; $i < $count; $i++) {
            $years[] = self::baseYear - $i;
        }

        return $years;
    }
}